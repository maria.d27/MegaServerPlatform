## MegaServerPlatform

* MegaServerPlatform is platform for running tasks on different workers.
* Clients connect to server and send tasks(files) to the server.
* Server collects tasks in the queue.
* Workers connect to the server and ask for a new task.
* Server send tasks to free workers and workers perform them.
* After worker was finished with the task, it update the server and ask for more tasks.

* Currently workers run only jar files - it is easy to change.(explained below).

* Commands to run - (Optionally:<#port_num_for_workers> <#port_num_for_clients> <#ip_address>):
	- java MegaServerPlatform server <#port_num_for_workers> <#port_num_for_clients>
	- java MegaServerPlatform worker <#port_num_for_workers> <#ip_address>
	- java MegaServerPlatform client <#port_num_for_clients> <#ip_address>

* The system use default values:
	- port_num_for_workers = 2230
	- port_num_for_clients = 2231
	- ip_address = 127.0.0.1

## How To Use:

	Demo client:
		You can create your own client that will send files to server or use exist demo client.
		Use exist Demo client:
			download hello.jar file and save local.
			update path of FILE_TO_SEND in ClientDemo.java to saved file
		
	Worker:
		currently it runs only JAR files.
		You can change it by following next steps:
			Go to ExecuteTask and create your own method.
			Call the method from worker.java in run function at mention place.
								
	Run Server on machine: (Optionally:<#port_num_for_workers> = 2230, <#port_num_for_clients> = 2231) 
		java MegaServerPlatform server <#port_num_for_workers> <#port_num_for_clients>
		
	Run Worker on machine: (Optionally:<#port_num_for_workers> = 2230, <#ip_address> = 127.0.0.1) 
		java MegaServerPlatform worker <#port_num_for_workers> <#ip_address>
		
	Run Demo client on machine: (Optionally:<#port_num_for_clients> = 2231, <#ip_address> = 127.0.0.1) 
		java MegaServerPlatform client <#port_num_for_clients> <#ip_address>
		
		

		
	
		
		
		
		