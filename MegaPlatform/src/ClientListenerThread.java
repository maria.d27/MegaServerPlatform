import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Listening for clients.
 * Create a client socket and client thread for each connection.
 * Save each client thread in threads array for future use.
 */
public class ClientListenerThread extends Thread  {
	
	private static int maxClientsCount;
	private static IConnectonThreads[] threads;
	private static Socket clientSocket = null;
	private static ServerSocket serverSocket = null;
	private int portNumber;
	 
	 
	 public ClientListenerThread(int maxClientsCount, int portNumber){
		 ClientListenerThread.maxClientsCount = maxClientsCount;
		 this.portNumber = portNumber;
		 threads = new ClientThread[maxClientsCount];		 
	 }
	 

	public void run() {
	
		//Open a server socket on the portNumber (default 2231).
	    try {
	      serverSocket = new ServerSocket(portNumber);   
	    } catch (IOException e) {
	      System.out.println(e);
	    }
	    	
	    //Create a client socket and client thread for each connection.
		while (true) {
			try {
				System.out.println("Waiting...  portNumber(for clients):" +  this.portNumber);		      
		    	  
		    	clientSocket = serverSocket.accept();
		        
		    	System.out.println("\nAccepted connection clientSocket:" + clientSocket);
		    	int i = 0;
		    	for (i = 0; i < maxClientsCount; i++) {
		    		if (threads[i] == null) {
		    			ClientThread client;
						(client = new ClientThread(clientSocket, (ClientThread[]) threads)).start();
						threads[i] = client;
						break;
		    		}
		        }
		        if (i == maxClientsCount) {
		        	PrintStream os = new PrintStream(clientSocket.getOutputStream());
		        	os.println("Server too busy. Try later.");
		        	os.close();
		        	clientSocket.close();
		        }	        
	      } catch (IOException e) {
	        System.out.println(e);
	      }	         
	    }		
	}
	
	
	 /**
	  * Removes unused thread of clients from threads array
	  * @param thread_to_remove
	  */
	 public static void removeThread(IConnectonThreads thread_to_remove){

		for (int i = 0; i < maxClientsCount; i++) {
			 if(thread_to_remove.equals(threads[i])) {
				 threads[i] = null;
				 break;
			 }
		 }
	 }

}
