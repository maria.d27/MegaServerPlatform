import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;


/*
 * Worker thread opens the input and the output
 * streams for a worker, get task from queue and send it to worker.
 */
class WorkerThread extends Thread implements IConnectonThreads{

  private InputStream is = null;
  private PrintStream os = null;
  private Socket clientSocket = null;
  private final WorkerThread[] threads;
  private int maxClientsCount;
  private OutputStream out = null;
  private  byte [] mybytearray = null;
  

  public WorkerThread(Socket clientSocket, WorkerThread[] threads) {
    this.clientSocket = clientSocket;
    this.threads = threads;
    maxClientsCount = threads.length;
  }

  public void run() {
    try {
    	
    	//Create input and output streams for this client.
		  is = clientSocket.getInputStream();
	      BufferedReader is_lines = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	      os = new PrintStream(clientSocket.getOutputStream());
	      
	      String is_ready="";
	      boolean ready = false;
	      while(true){
	    	  	    		
	    	  //check if the worker is ready to get new task
	    	  if(!ready){
	    	    	is_ready = is_lines.readLine().trim();
	    	  }
		    	  
	    	  if(is_ready.equals("ready")){	    		  
	    		  ready = true;  	  

	    		  mybytearray =   QueueOfTasks.getInstance().RemoveTaskFromQueue();
	    		  if(mybytearray != null ){
				      out = clientSocket.getOutputStream();				      
				      System.out.println("Sending " + "(" + mybytearray.length + " bytes)");				      
				      out.write(mybytearray,0,mybytearray.length);
				      out.flush();
				      System.out.println("Done - send task to worker.");
				      ready = false;
	    		  }
	    	  }
    		}   

    } catch (IOException e) {
    	QueueOfTasks.getInstance().addTaskToQueue(mybytearray);
    	System.out.println(e);
    }
    finally {
    		WorkerListenerThread.removeThread(this); 
			try {
			      is.close();
			      os.close();
			      clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	   
      }
  }
}