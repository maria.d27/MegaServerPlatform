/**
 * 
 * Interface of server and worker
 *
 */
public interface IService {

	/**
	 * set configuration - port and ip_address
	 * @param args
	 */
	public void setConfiguration(String[] args);
	
	/**
	 * Define and Run the connection 
	 */
	public void connection();
	
}
