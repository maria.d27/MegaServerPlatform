/**
 * Define how many clients and workers can connect - default value = 30.
 * Creates 2 listener threads one for clients and one for workers.
 */
public class Server implements IService {

  private int portNumber_to_send_tasks;
  private int portNumber_to_get_tasks;  
  
  public Server(){ 	  
	  this.setPortNumber(2230,2231);
  }
  
  @Override
  public void setConfiguration(String[] args) {
	  
	  if (args.length > 2) {
			  portNumber_to_send_tasks = Integer.valueOf(args[1]).intValue();
	          portNumber_to_get_tasks = Integer.valueOf(args[2]).intValue();
	      		  
		  System.out.println("Usage: Server <portNumber>\n"
				  + "portNumber_to_send_tasks=" + this.portNumber_to_send_tasks
				  + "portNumber_to_get_tasks=" + this.portNumber_to_get_tasks  
				  
				  );
		  		  
		  this.setPortNumber(portNumber_to_send_tasks,portNumber_to_get_tasks);
	  }
  }
  
  
  /**
   * Create 2 threads
   * 	ClientListenerThread - listen to clients, as new client connect open new thread
   *  	WorkerListenerThread - listen to workers, as new client connect open new thread
   */
  @Override
  public void connection() {	  
	 (new WorkerListenerThread(30,portNumber_to_send_tasks)).start();
	 (new ClientListenerThread(30,portNumber_to_get_tasks)).start();
  }
  
   
/**
 * 
 * @param portNumber_to_send_tasks
 * @param portNumber_to_get_tasks
 */
  public void setPortNumber(int portNumber_to_send_tasks,int portNumber_to_get_tasks) {
	this.portNumber_to_send_tasks = portNumber_to_send_tasks;
	this.portNumber_to_get_tasks = portNumber_to_get_tasks;
  }

}

