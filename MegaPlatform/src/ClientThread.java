import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;


/**
 * Client thread opens the input and the output
 * streams for a client.
 * Get file from client and save it to the queue.
 * maxClientsCount and threads for future use.
 */
class ClientThread extends Thread implements IConnectonThreads{

  private InputStream is = null;
  private Socket clientSocket = null;
  private final ClientThread[] threads;
  private int maxClientsCount;
  public final static int FILE_SIZE = 6022386;

  
  public ClientThread(Socket clientSocket, ClientThread[] threads) {
    this.clientSocket = clientSocket;
    this.threads = threads;
    maxClientsCount = threads.length;
  }

  public void run() {
	    try {
	    	//Create input streams for this client.
			  is = clientSocket.getInputStream();
		
			  
			  //Gets task and save it to queue
		      ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		      byte[] buf = new byte[FILE_SIZE];
		      while(true) {
		    	  int n = is.read(buf);
		    	  if( n < 0 ) break;
		    	  baos.write(buf,0,n);
		      }
		
		      byte task[] = baos.toByteArray();
		      QueueOfTasks.getInstance().addTaskToQueue(task); 
		      System.out.println("Done - get task from client.");
		
	    } catch (IOException e) {
    }
	    finally {
	    	//remove thread from threads 
    		ClientListenerThread.removeThread(this); 
			try {
			      is.close();
			      clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	   
      }
  }
}