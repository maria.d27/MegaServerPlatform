import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/** 
 * Demonstrate client - connects to server and send jar File 
 * FILE_TO_SEND - set path of jar file
 */
public class ClientDemo implements Runnable, IService {

  // The client socket
  private static Socket clientSocket = null;
  // The output stream
  private static PrintStream os = null;
  // The input stream
  private static InputStream is = null;
  private static BufferedReader is_lines = null;

  public final static String FILE_TO_SEND = "E:/MegaServerPlatform/Hello/src/hello.jar";
  public final static int FILE_SIZE = 6022386;
  
  private int portNumber;
  private String host;
  
  public ClientDemo(){
	  this.setHost("localhost");
	  this.setPortNumber(2231);
  }
  
  
  @Override
  public void setConfiguration(String[] args) {
	  
	   if (args.length > 2) {
	   	  this.portNumber = Integer.valueOf(args[1]).intValue();
	   	  this.host = args[2];
	   }
	 
	  System.out
	  .println("Usage: Worker <host> <portNumber>\n"
	   		+ "Now using host=" + this.host + ", portNumber=" + this.portNumber);
	    
	  if(!this.host.isEmpty()){
		  this.setHost(this.host);
	  }
	   
	  this.setPortNumber(this.portNumber);
  }
  
  
  public void connection() {

	  //Open a socket on a given host and port. Open input and output streams.
    try {
      clientSocket = new Socket(host, portNumber);
      System.out.print("worker run");
      os = new PrintStream(clientSocket.getOutputStream());
	  is = clientSocket.getInputStream();
      is_lines = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	  
	  
    } catch (UnknownHostException e) {
      System.err.println("Don't know about host " + host);
    } catch (IOException e) {
      System.err.println("Couldn't get I/O for the connection to the host "
          + host);
    }

    if (clientSocket != null && os != null && is_lines != null) {
        new Thread(new ClientDemo()).start();
    }
  }

  /*
   * Create a thread to read from the client. 
   */
  public void run() {
  
    try {
    	
	      File myFile = new File (FILE_TO_SEND);
	      byte [] mybytearray  = new byte [(int)myFile.length()];		   
	      FileInputStream fis = new FileInputStream(myFile);
	      BufferedInputStream  bis = new BufferedInputStream(fis);
	      bis.read(mybytearray,0,mybytearray.length);
		  
	      java.io.OutputStream out = clientSocket.getOutputStream();
	      System.out.println("Sending " + FILE_TO_SEND + "(" + mybytearray.length + " bytes)");
	      out.write(mybytearray,0,mybytearray.length);
	      out.flush();
    

	      if (fis != null) fis.close();
	      if (bis != null) bis.close();
	      if (out != null) out.close();
	      os.close();
	      is_lines.close();
	      clientSocket.close();
	      	      
	      System.out.print("Task was sent to server");
    } catch (IOException e) {
      System.err.println("IOException:  " + e);
    }
    
    
  }


  public int getPortNumber() {
		return portNumber;
	}


	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}


	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		this.host = host;
	}

}