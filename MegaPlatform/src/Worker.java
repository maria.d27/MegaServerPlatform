import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * 
 * Worker gets task from server.
 * Write it to file - ./FilesOfWorkers/worker.jar.
 * Perform the task.
 */
public class Worker implements Runnable, IService {

  // The client socket
  private static Socket clientSocket = null;
  // The output stream
  private static PrintStream os = null;
  // The input stream
  private static InputStream is = null;
  private static BufferedReader is_lines = null;

  private static InputStream inputLine = null;
  private static BufferedReader inputLine_lines = null;
  
  //-------------------file----------------------
  public String FILE_TO_RECEIVED = "./FilesOfWorkers/worker.jar";
  public static int workersAmount = 0;
  public int workerIndex;
  public final static int FILE_SIZE = 6022386;
  //-------------------------------------------
  
  private int portNumber;
  private String host;
  
  public Worker(){
	  this.setHost("localhost");
	  this.setPortNumber(2230);
  }
  

  
  @Override
  public void setConfiguration(String[] args) {
	  
	  if (args.length > 2) {
		  this.portNumber = Integer.valueOf(args[1]).intValue();
		  this.host = args[2];
	  }
	 
	  System.out
	    .println("Worker using host=" + this.host + ", portNumber=" + this.portNumber+"\n");
	    
	  if(!this.host.isEmpty()){
		  this.setHost(this.host);
	  }
	  
	  this.setPortNumber(this.portNumber);
  }
  
  
  public void connection() {

	//Open a socket on a given host and port. Open input and output streams.
    try {
      clientSocket = new Socket(host, portNumber);
      System.out.println("worker connect \n");
	  inputLine = new BufferedInputStream(System.in);
      inputLine_lines =  new BufferedReader(new InputStreamReader(inputLine, "UTF-8"));	  
      os = new PrintStream(clientSocket.getOutputStream());
	  is = clientSocket.getInputStream();
      is_lines = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	  	  
    } catch (UnknownHostException e) {
      System.err.println("Don't know about host " + host);
    } catch (IOException e) {
      System.err.println("Couldn't get I/O for the connection to the host "
          + host);
    }
    
    //opened a connection to on the port portNumber.
    if (clientSocket != null && os != null && is_lines != null) {
        // Create a thread to read from the server.
        new Thread(new Worker()).start();      
    }
  }

  /**
   * Create a thread to read from the server.
   */
  public void run() {

    FileOutputStream fos = null;
    BufferedOutputStream bos = null;
    try {

    	//send to server that the worker is ready
    	 os.println("ready");	

    	 //Keep read on from the socket
    	 while(true){
    		 	        
	        int bytesRead;
	        int current = 0;
	        byte [] mybytearray  = new byte [FILE_SIZE];
	        
	        bytesRead = is.read(mybytearray,0,mybytearray.length);
	        
	        //if success to read
	        if(bytesRead >-1){
		        current = bytesRead;	
		        fos = null;
		        bos = null;
		        fos = new FileOutputStream(FILE_TO_RECEIVED);
		        bos = new BufferedOutputStream(fos);		        
		        bos.write(mybytearray, 0 , current);
		        bos.flush();
		        		        
		        System.out.println("Execute task (" + current + " bytes read):");
		        
		        // call method that run JAR files
		        // ================================================
		        //Here you can call other method to run other tasks
		        //==================================================
		        ExecuteTask.runJARFiles(FILE_TO_RECEIVED);
		        	
		        //send to server that the worker is ready
		        os.println("ready");	        
	        }
    	 }	        
    } catch (IOException e) {
      System.err.println("IOException:  " + e);
    }
    finally {
		try {
			if (os != null)	os.close();
			 if (is != null) is.close();
			 if (fos != null) fos.close();
			 if (bos != null) bos.close();
			 if (clientSocket != null) clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}			
    }       
  }


  public int getPortNumber() {
		return portNumber;
  }

  public void setPortNumber(int portNumber) {
	this.portNumber = portNumber;
  }

  public String getHost() {
	return host;
  }

  public void setHost(String host) {
	this.host = host;
  }

}