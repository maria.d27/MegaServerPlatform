import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

/** 
 * Singleton design pattern
 * Handles add,get, remove items from queue.
 * Use lock to avoid add and remove action at the same time.
 */
public class QueueOfTasks {
	
	   private static QueueOfTasks queue = new QueueOfTasks();	   
	   private final static ReentrantLock  queueLock = new ReentrantLock();
	   private static Queue<byte[]> tasksQueue = new LinkedList<byte[]>();
	   
	   /**
	    * Prevents any other class from instantiating.
	    */
	   private QueueOfTasks() { }

	   /** Static 'instance' method */
	   public static QueueOfTasks getInstance( ) {
		   return queue;
	   }

	   /**
	    * get tasksQueue
	    * @return Queue<byte[]>
	    */
	   public Queue<byte[]> getQueue() {
		   return tasksQueue;
	   }

	   
	   /**
	    * remove task from tasksQueue
	    * @return byte[]
	    */
		public byte[] RemoveTaskFromQueue() {
			queueLock.lock();
			byte[] task = tasksQueue.poll();	 
			queueLock.unlock();		 
			return task;
		}
		
	   /**
	    * get task from tasksQueue
	    * @return byte[]
	    */
		public byte[] getTaskFromQueue() {			
			return tasksQueue.peek();			
		}
		
	   /**
	    * add task to tasksQueue
	    */
		public void addTaskToQueue(byte[] task) {
			queueLock.lock();
			tasksQueue.add(task);
			queueLock.unlock();	
		}
		
}