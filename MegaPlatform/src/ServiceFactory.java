/**
 * 
 * Factory design pattern
 *
 */
public class ServiceFactory {

	public IService getService(String serviceType){
			
	     if(serviceType == null){
	         return null;
	      }		
	      if(serviceType.equalsIgnoreCase("Server")){
	         return new Server();	         
	         
	      } else if(serviceType.equalsIgnoreCase("Worker")){
	         return new Worker();
	      } 
	      //for ClientDemo use
	      else if(serviceType.equalsIgnoreCase("Client")){
		         return new ClientDemo();
		      } 
	      return null;
	}
}
