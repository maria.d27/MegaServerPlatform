import java.io.IOException;
import java.io.InputStream;

/**
 * Execute tasks - currently only JAR files
 */
public class ExecuteTask {
	
	/**
	 * Process that run jar file
	 * @param FILE_TO_RECEIVED
	 */
	public static void runJARFiles(String FILE_TO_RECEIVED){
		
		//Process that run jar file
        Process proc;
		try {
			proc = Runtime.getRuntime().exec("java -jar " + FILE_TO_RECEIVED);
			proc.waitFor();
	      
	        InputStream in = proc.getInputStream();
	        InputStream err = proc.getErrorStream();
	
	        byte b[]=new byte[in.available()];
	        in.read(b,0,b.length);
	        System.out.println(new String(b));
	
	        byte c[]=new byte[err.available()];
	        err.read(c,0,c.length);
	        System.out.println(new String(c));
        
		} catch (InterruptedException e) {
			e.printStackTrace();
		}catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
