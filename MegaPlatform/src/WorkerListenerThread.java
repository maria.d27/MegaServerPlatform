import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Listening for workers.
 * Create a worker socket and worker thread for each connection.
 * Save each worker thread in threads array for future use.
 */
public class WorkerListenerThread extends Thread  {
	
	 private static int maxClientsCount;
	 private static IConnectonThreads[] threads;// = new workerThread[maxClientsCount];
	 private static Socket clientSocket = null;
	 private static ServerSocket serverSocket = null;
	 private int portNumber;
	 
	 
	 public WorkerListenerThread(int maxClientsCount, int portNumber){
		 WorkerListenerThread.maxClientsCount = maxClientsCount;
		 this.portNumber = portNumber;
		 threads = new WorkerThread[maxClientsCount];	 
	 }
	 

	 
	public void run() {

		//Open a server socket on the portNumber (default 2230). 
	    try {
	      serverSocket = new ServerSocket(portNumber);	      
	    } catch (IOException e) {
	      System.out.println(e);
	    }
	    
	    //Create a worker socket and worker thread for each connection.
	    while (true) {
	    	try {
	    		System.out.println("Waiting... portNumber(for workerks):" +  this.portNumber);	      
		    	  
		    	clientSocket = serverSocket.accept();
		        		        		        
		        System.out.println("\nAccepted connection workerSocket :" + clientSocket);
		        int i = 0;
		        for (i = 0; i < maxClientsCount; i++) {
		        	if (threads[i] == null) {
						WorkerThread worker ;
						(worker = new WorkerThread(clientSocket, (WorkerThread[]) threads)).start();
			   			threads[i] = worker;
			   			break;
		          }
		        }
		        if (i == maxClientsCount) {
		        	PrintStream os = new PrintStream(clientSocket.getOutputStream());
		        	os.println("Server too busy. Try later.");
		        	os.close();
		        	clientSocket.close();
		        }        
	      } catch (IOException e) {
	        System.out.println(e);
	      }
	    }
		
	}
	 
	 /**
	  * Removes unused thread of workers from threads array
	  * @param thread_to_remove
	  */
	 public static void removeThread(IConnectonThreads thread_to_remove){
		for (int i = 0; i < maxClientsCount; i++) {
			 if(thread_to_remove.equals(threads[i])) {
				 threads[i] = null;
				 break;
			 }
		 }
	 }
}
