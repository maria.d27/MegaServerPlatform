/**
 * 
 * @author Maria Dorohin 2016
 * 
 * MegaServerPlatform is platform for running tasks on different workers.
 * Clients connect to server and send tasks(files) to server.
 * Server collect tasks in the queue.
 * Workers connect to the server and ask for a new task.
 * Server send tasks to free workers and workers perform them.
 * After worker was finished with the task, it update the server and ask for more tasks.
 * 
 * Currently workers run only jar files - it is easy to change:
 * 	Go to ExecuteTask and create you own method
 * 	Call the method from worker.java in run function at mention place.
 *  
 * Commands to run:
 * 	java MegaServerPlatform server <#port_num_for_workers> <#port_num_for_clients>
 * 	java MegaServerPlatform worker <#port_num_for_workers> <#ip_address>
 * 	java MegaServerPlatform client <#port_num_for_clients> <#ip_address>
 *
 * MegaServerPlatform use default values:
 *	port_num_for_workers = 2230
 *	port_num_for_clients = 2231
 *	ip_address = 127.0.0.1
 *
 */
public class MegaServerPlatform {

	public static void main(String[] args) {
		
		/**
		 * Gets instance of worker or server according to args[] 
		 * by using of factory design pattern.
		 * 
		 */
		ServiceFactory serviceFactory = new ServiceFactory();		
		IService service = serviceFactory.getService(args[0]);		  
		service.setConfiguration(args);
		service.connection();		 
	}

}
